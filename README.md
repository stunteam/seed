# Stunteam's seed project

Common configuration for all Stunteam's projects

### Use

Go to your exiting project directory and run this in Bash to download latest master branch and extract it to current directory without file overwrite.

```bash
curl -sL https://get.stun.team/seed \
| tar xzk --strip 1 --ignore-case --exclude *.md
```
